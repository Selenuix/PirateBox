#!/bin/sh

#  Matthias Strubel (c) 2014,2016  - GPL3
#  matthias.strubel@aod-rpg.de
#
#  Ce sript supprime les fichiers de 0 octets
#  Ce script est nécéssaire pour la Piratebox sur OpenWRT
#   The find utility there has only a limited feature set.

IFS='
'


# Changer le répertoire, s'il n'existe pas, sortir pour ne pas nettoyer le
# Système de fichiers OS.
cd $1   || exit 1

ls_list=$( find ./ -type f )

for filename in $ls_list  
do
   if [ ! -s $filename ] ; then
      echo "Suppression de 0 Byte file $filename"
      rm $filename
   fi
done
