<!DOCTYPE html>
<html>
	<head>
		<title>PirateBox - Partager librement !</title>
		<script src="/content/js/jquery.min.js"></script>
		<script src="/content/js/scripts.js"></script>
		<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width">
	</head>
	<body>
		<header id="header">
			<div class="container">
				<div id="logo">
					<h1>
						<a href="/">
							<img src="/content/img/piratebox-logo-horizontal-white.png" alt="PirateBox" title="PirateBox - Partager librement !">
						</a>
					</h1>
				</div>
				<div id="menu-icon"><img src="/content/img/menu.png" alt="Menu"></div>
				<nav id="top-nav">
					<ul>
						<li><a href="/">Accueil</a></li>
						<li><a href="/board/">Forum</a></li>
						<li><a href="/Shared/" class="current">Fichiers</a></li>
						<li><a href="#about">À propos</a></li>
					</ul>
				</nav>
			</div>
		</header>
		<div class="container">
			<div class="card">
		