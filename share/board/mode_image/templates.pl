use strict;

BEGIN { require 'wakautils.pl' }



#
# Chaîne de caractère de l'interface
#

use constant S_HOME => 'Accueil';												# Retour à la page d'accueil.
use constant S_RETURN => 'Retour';												# Retour au tableau d'image.
use constant S_POSTING => 'Mode de réponse';									# Affiche le message dans la barre rouge au dessus de l'écran de réponse.

use constant S_NAME => 'Nom';													# Décrit le champ nom.
use constant S_EMAIL => 'Lien';													# Décrit le champ email.
use constant S_SUBJECT => 'Sujet';												# Décrit le sujet.
use constant S_SUBMIT => 'Envoyer';												# Décrit le boutton d'envoi.
use constant S_COMMENT => 'Commenter';											# Décrit le champ commenter.
use constant S_UPLOADFILE => 'Fichier';											# Décrit  le champ fichier.
use constant S_CAPTCHA => 'Vérification';										# Décrit le champ du captcha.
use constant S_DELPASS => 'Mot de passe';										# Décrit le champ mot de passe.
use constant S_DELEXPL => '(pour la suppression du post et de l\'image)';		# Affiche l'explication pour la boîte de mot de passe (à droite).

use constant S_THUMB => 'La miniature s\'affiche, cliquez sur l\'image pour la taille réelle.';		# Affiche les instructions pour visualiser la source réelle.
use constant S_HIDDEN => 'Miniature caché, cliquez sur le nom du fichier pour l\'image complète.';	# Affiche les instructions pour afficher la réponse d'image cachée.
use constant S_NOTHUMB => 'Pas<br />de miniature';													# Afficher lorsqu'il n'y a pas de miniature.
use constant S_PICNAME => 'Fichier : ';																# Texte affiché avant de télécharger le nom / lien.
use constant S_REPLY => 'Répondre';																	# Texte affiché pour la réponse.
use constant S_ABBR => '%d posts omis. Cliquez sur Répondre pour voir.';							# Texte à afficher lorsque les réponses sont cachées.
use constant S_ABBRIMG => '%d posts et %d images omis. Cliquez sur répondre pour voir.';			# Texte à afficher lorsque les réponses et les images sont cachées.
use constant S_ABBRTEXT => 'Commentaire trop long. Cliquez <a href="%s">ici</a> pour voir plus.';

use constant S_REPDEL => 'Supprimer le post ';														# Prints text next to S_DELPICONLY (left)
use constant S_DELPICONLY => 'Fichier uniquement';													# Prints text next to checkbox for file deletion (right)
use constant S_DELKEY => 'Mot de passe';															# Prints text next to password field for deletion (left)
use constant S_DELETE => 'Supprimer';																# Defines deletion button's name

use constant S_PREV => 'Précédent';																	# Defines previous button
use constant S_FIRSTPG => 'Précédent';																# Defines previous button
use constant S_NEXT => 'Suivant';																	# Defines next button
use constant S_LASTPG => 'Suivant';																	# Defines next button

use constant S_FRONT => 'Accueil';																	# Title of the front page in page list

#
# Messages d'erreurs
#

use constant S_BADCAPTCHA => 'Code de vérification incorrect.';								# Message d'erreur quand le captcha est faux.
use constant S_UNJUST => 'Les posts doivent être effectués à travers une requête POST';		# Message d'erreur quand un post force GET ou une autre méthode que POST.

use constant S_NOTEXT => 'Vous n\'avez pas entré de texte.';								# Message d'erreur quand aucun texte n'est entré.
use constant S_NOTITLE => 'Vous n\'avez pas entré de texte.';								# Message d'erreur quand aucun titre n'est entré.
use constant S_NOTALLOWED => 'Vous n\'avez pas la permission de poster.';					# Message d'erreur quand le type de post est interdi pour les non-admins.
use constant S_TOOLONG => 'Le champs %s est trop long, de %d caractères.';					# Message d'erreur quand il y a trop de caractères dans le champs.
use constant S_UNUSUAL => 'Réponse anormale.';												# Message d'erreur, la réponse est anormale (c'est un mystère !).
use constant S_SPAM => 'Le spam est interdit !';											# Message d'erreur quand du spam est détecté.
use constant S_THREADCOLL => 'Quelqu\'un d\'autre essaie de poster en même temps.';			# Si deux personnes créent un post en même temps.
use constant S_NOTHREADERR => 'Le thread spécifié n\'existe pas.';							# Message d'erreur quand un post qui n'existe pas est appelé.
use constant S_BADDELPASS => 'Mot de passe incorrect.';										# Message d'erreur pour mot de passe incorrect (quand l'utilisateur essaie de supprimer un fichier).
use constant S_NOTWRITE => 'Le dossier n\'est pas accessible en écriture.';					# Message d'erreur quand le dossier n'est pas accessible en écriture, chmod (777) est faux.
use constant S_NOTASK => 'Erreur du script; Aucune tâche valide spécifiée.';				# Message d'erreur quand le script est appelé de manière incorrecte.
use constant S_NOLOG => 'Impossible d\'écrire dans log.txt.';								# Message d'erreur quand le script ne peux pas écrire dans les logs.
use constant S_TOOBIG => 'Le fichier que vous essayez d\'envoyer est trop lourd.';		  # Message d'erreur quand la taille du fichier est > à MAX_KB.
use constant S_EMPTY => 'Le fichier que vous essayez d\'envoyez est vide.';					# Message d'erreur quand la taille du fichier est de 0 octets
use constant S_BADFORMAT => 'Format de fichier non autorisé.';								# Message d'erreur quand le type de fichier n'est pas supporté
use constant S_DUPE => 'Le fichier à déjà été posté <a href="%s">içi</a>.';
					# Message d'erreur quand le checksum md5 existe déjà.
use constant S_DUPENAME => 'Un fichier avec le même nom existe déjà.';
					# Message d'erreur quand deux fichiers ont le même nom.
use constant S_THREADCLOSED => 'Ce thread est fermé.';					
					# Message d'erreur quand le thread est fermé.



#
# Templates
#

use constant NORMAL_HEAD_INCLUDE => q{

<!DOCTYPE html>
<html lang="fr">
	<head>
		<title><if $title><var $title> - </if><const TITLE></title>
		<meta http-equiv="Content-Type" content="text/html;charset=<const CHARSET>" />
		<link rel="shortcut icon" href="<const expand_filename(FAVICON)>" />

		<style type="text/css">
			body { margin: 0; padding: 12px; margin-bottom: auto; }
			blockquote blockquote { margin-left: 0em }
			form { margin-bottom: 0px }
			.postarea { text-align: center }
			.postarea table { margin: 0px auto; text-align: left }
			.thumb { border: none; float: left; margin: 2px 20px }
			.nothumb { float: left; background: #eee; border: 2px dashed #aaa; text-align: center; margin: 2px 20px; padding: 1em 0.5em 1em 0.5em; }
			.reply blockquote, blockquote :last-child { margin-bottom: 0em }
			.reflink a { color: inherit; text-decoration: none }
			.reply .filesize { margin-left: 20px }
			.userdelete { float: right; text-align: center; white-space: nowrap }
			.replypage .replylink { display: none }
		</style>

		<loop $stylesheets>
			<link rel="<if !$default>alternate </if>stylesheet" type="text/css" href="<var expand_filename($filename)>" title="<var $title>" />
		</loop>

		<script type="text/javascript">var style_cookie="<const STYLE_COOKIE>";</script>
		<script type="text/javascript" src="<const expand_filename(JS_FILE)>"></script>
		<script type="text/javascript">require_script_version("3.a");</script>
	</head>
		<if $thread><body class="replypage"></if>
		<if !$thread><body class="mainpage"></if>

}.include(INCLUDE_DIR."header.html").q{

	<div class="adminbar">
		<loop $stylesheets>
			[<a href="javascript:set_stylesheet('<var $title>')"><var $title></a>]
		</loop>
	-
		[<a href="<var expand_filename("..")>" target="_top"><const S_HOME></a>]
	</div>

	<div class="logo">
		<if SHOWTITLEIMG==1><img src="<var expand_filename(TITLEIMG)>" alt="<const TITLE>" /></if>
		<if SHOWTITLEIMG==2><img src="<var expand_filename(TITLEIMG)>" onclick="this.src=this.src;" alt="<const TITLE>" /></if>
		<if SHOWTITLEIMG and SHOWTITLETXT><br /></if>
		<if SHOWTITLETXT><const TITLE></if>
	</div><hr />
};

use constant NORMAL_FOOT_INCLUDE => include(INCLUDE_DIR."footer.html").q{

</body></html>
};

use constant MAIN_PAGE_TEMPLATE => compile_template(NORMAL_HEAD_INCLUDE.q{

<if ALLOW_TEXT_THREADS or ALLOW_IMAGE_THREADS>
	<div class="postarea">
	<form id="postform" action="<var $self>" method="post" enctype="multipart/form-data">

	<input type="hidden" name="task" value="post" />
	<if FORCED_ANON><input type="hidden" name="field_a" /></if>

	<table><tbody>
	<if !FORCED_ANON><tr><td class="postblock"><const S_NAME></td><td><input type="text" name="field_a" size="28" /></td></tr></if>
	<tr><td class="postblock"><const S_EMAIL></td><td><input type="text" name="field_b" size="28" /></td></tr>
	<tr><td class="postblock"><const S_SUBJECT></td><td><input type="text" name="title" size="35" />
	<input type="submit" value="<const S_SUBMIT>" /></td></tr>
	<tr><td class="postblock"><const S_COMMENT></td><td><textarea name="comment" cols="48" rows="4"></textarea></td></tr>

	<if ALLOW_IMAGE_THREADS>
		<tr><td class="postblock"><const S_UPLOADFILE></td><td><input type="file" name="file" size="35" />
		</td></tr>
	</if>

	<if ENABLE_CAPTCHA>
		<tr><td class="postblock"><const S_CAPTCHA></td><td><input type="text" name="captcha" size="10" />
		<img alt="" src="<var expand_filename('captcha.pl')>" />
		</td></tr>
	</if>

	<tr><td class="postblock"><const S_DELPASS></td><td><input type="password" name="password" size="8" /> <const S_DELEXPL></td></tr>

	<if SPAM_TRAP>
		<tr style="display:none">
		<td class="postblock"><const S_SPAMTRAP></td>
		<td><input type="text" name="name" size="10" autocomplete="off" /><input type="text" name="link" size="10" autocomplete="off" /></td>
		</tr>
	</if>

	<tr><td colspan="2">
	<div class="rules">}.include(INCLUDE_DIR."rules.html").q{</div></td></tr>
	</tbody></table></form></div>
	<script type="text/javascript">set_new_inputs("postform")</script>
</if>

<hr />
<form id="delform" action="<var $self>" method="post">

<loop $threads>
	<loop $posts>
		<var $text>

		<if $abbreviated><div class="abbrev"><var sprintf(S_ABBRTEXT,"$self/$thread/$num","$self/$thread/")></div></if>
		<if $omit and $num==1>
			<span class="omittedposts">
			<if $omitimages><var sprintf S_ABBRIMG,$omit,$omitimages></if>
			<if !$omitimages><var sprintf S_ABBR,$omit></if>
			</span>
		</if>
	</loop>
	<br clear="left" /><hr />
</loop>

<table class="userdelete"><tbody><tr><td>
<input type="hidden" name="task" value="delete" />
<const S_REPDEL>[<label><input type="checkbox" name="fileonly" value="on" /><const S_DELPICONLY></label>]<br />
<const S_DELKEY><input type="password" name="password" size="8" />
<input value="<const S_DELETE>" type="submit" /></td></tr></tbody></table>
</form>
<script type="text/javascript">set_delpass("delform")</script>

<table border="1"><tbody><tr><td>

<if $prevpage><form method="get" action="<var $prevpage>"><input value="<const S_PREV>" type="submit" /></form></if>
<if !$prevpage><const S_FIRSTPG></if>

</td><td>

<loop $pages>
	<if $page ne $current>[<a href="<var $filename>"><var $page></a>]</if>
	<if $page eq $current>[<var $page>]</if>
</loop>

</td><td>

<if $nextpage><form method="get" action="<var $nextpage>"><input value="<const S_NEXT>" type="submit" /></form></if>
<if !$nextpage><const S_LASTPG></if>

</td></tr></tbody></table><br clear="all" />

}.NORMAL_FOOT_INCLUDE,KEEP_MAINPAGE_NEWLINES);



use constant THREAD_HEAD_TEMPLATE => compile_template(NORMAL_HEAD_INCLUDE.q{

[<a href="<var expand_filename(HTML_SELF)>"><const S_RETURN></a>]
<div class="theader"><const S_POSTING></div>

<if ALLOW_TEXT_REPLIES or ALLOW_IMAGE_REPLIES>
	<div class="postarea">
	<form id="postform" action="<var $self>" method="post" enctype="multipart/form-data">

	<input type="hidden" name="task" value="post" />
	<input type="hidden" name="thread" value="<var $thread>" />
	<if FORCED_ANON><input type="hidden" name="field_a" /></if>

	<table><tbody>
	<if !FORCED_ANON><tr><td class="postblock"><const S_NAME></td><td><input type="text" name="field_a" size="28" /></td></tr></if>
	<tr><td class="postblock"><const S_EMAIL></td><td><input type="text" name="field_b" size="28" /></td></tr>
	<tr><td class="postblock"><const S_SUBJECT></td><td><input type="text" name="title" size="35" />
	<input type="submit" value="<const S_SUBMIT>" /></td></tr>
	<tr><td class="postblock"><const S_COMMENT></td><td><textarea name="comment" cols="48" rows="4"></textarea></td></tr>

	<if ALLOW_IMAGE_REPLIES>
		<tr><td class="postblock"><const S_UPLOADFILE></td><td><input type="file" name="file" size="35" /></td></tr>
	</if>

	<if ENABLE_CAPTCHA>
		<tr><td class="postblock"><const S_CAPTCHA></td><td><input type="text" name="captcha" size="10" />
		<img alt="" src="<var expand_filename('captcha.pl')>?dummy=<var $size>" />
		</td></tr>
	</if>

	<tr><td class="postblock"><const S_DELPASS></td><td><input type="password" name="password" size="8" /> <const S_DELEXPL></td></tr>

	<if SPAM_TRAP>
		<tr style="display:none">
		<td class="postblock"><const S_SPAMTRAP></td>
		<td><input type="text" name="name" size="10" autocomplete="off" /><input type="text" name="link" size="10" autocomplete="off" /></td>
		</tr>
	</if>

	<tr><td colspan="2">
	<div class="rules">}.include(INCLUDE_DIR."rules.html").q{</div></td></tr>
	</tbody></table></form></div>
	<script type="text/javascript">set_new_inputs("postform")</script>
</if>

<hr />
<form id="delform" action="<var $self>" method="post">

});



use constant THREAD_FOOT_TEMPLATE => compile_template(q{

<br clear="left" /><hr />

<table class="userdelete"><tbody><tr><td>
<input type="hidden" name="task" value="delete" />
<const S_REPDEL>[<label><input type="checkbox" name="fileonly" value="on" /><const S_DELPICONLY></label>]<br />
<const S_DELKEY><input type="password" name="password" size="8" />
<input value="<const S_DELETE>" type="submit" /></td></tr></tbody></table>
</form>
<script type="text/javascript">set_delpass("delform")</script>

}.NORMAL_FOOT_INCLUDE);



use constant REPLY_TEMPLATE => compile_template( q{
<if $num==1>
	<if $image>
		<span class="filesize"><const S_PICNAME><a target="_blank" href="<var expand_filename(clean_path($image))>"><var get_filename($image)></a>
		-(<em><var $size> B, <var $width>x<var $height></em>)</span>
		<span class="thumbnailmsg"><const S_THUMB></span><br />

		<if $thumbnail>
			<a target="_blank" href="<var expand_filename(clean_path($image))>">
			<img src="<var expand_filename($thumbnail)>" width="<var $tn_width>" height="<var $tn_height>" alt="<var $size>" class="thumb" /></a>
		</if>
		<if !$thumbnail>
			<div class="nothumb"><a target="_blank" href="<var expand_filename(clean_path($image))>"><const S_NOTHUMB></a></div>
		</if>
	</if>

	<a name="<var $num>"></a>
	<label><input type="checkbox" name="delete" value="<var $thread>,<var $num>" />
	<span class="filetitle"><var $title></span>
	<if $link><span class="postername"><a href="<var $link>"><var $name></a></span><if $trip><span class="postertrip"><a href="<var $link>"><if !$capped><var $trip></if><if $capped><var $capped></if></a></span></if></if>
	<if !$link><span class="postername"><var $name></span><if $trip><span class="postertrip"><if !$capped><var $trip></if><if $capped><var $capped></if></span></if></if>
	<var $date></label>
	<span class="reflink">
	<a href="javascript:w_insert('&gt;&gt;<var $num>','<var $self>/<var $thread>/')">No.<var $num></a>
	</span>&nbsp;
	<span class="replylink">[<a href="<var $self>/<var $thread>/" id="reply<var $thread>"><const S_REPLY></a>]</span>

	<blockquote>
	<var $comment>
	</blockquote>
</if>
<if $num!=1>
	<table><tbody><tr><td class="doubledash">&gt;&gt;</td>
	<td class="reply" id="reply<var $num>">

	<a name="<var $num>"></a>
	<label><input type="checkbox" name="delete" value="<var $thread>,<var $num>" />
	<span class="replytitle"><var $title></span>
	<if $link><span class="commentpostername"><a href="<var $link>"><var $name></a></span><if $trip><span class="postertrip"><a href="<var $link>"><if !$capped><var $trip></if><if $capped><var $capped></if></a></span></if></if>
	<if !$link><span class="commentpostername"><var $name></span><if $trip><span class="postertrip"><if !$capped><var $trip></if><if $capped><var $capped></if></span></if></if>
	<var $date></label>
	<span class="reflink">
	<a href="javascript:w_insert('&gt;&gt;<var $num>','<var $self>/<var $thread>/')">No.<var $num></a>
	</span>&nbsp;

	<if $image>
		<br />
		<span class="filesize"><const S_PICNAME><a target="_blank" href="<var expand_filename(clean_path($image))>"><var get_filename($image)></a>
		-(<em><var $size> B, <var $width>x<var $height></em>)</span>
		<span class="thumbnailmsg"><const S_THUMB></span><br />

		<if $thumbnail>
			<a target="_blank" href="<var expand_filename(clean_path($image))>">
			<img src="<var expand_filename($thumbnail)>" width="<var $tn_width>" height="<var $tn_height>" alt="<var $size>" class="thumb" /></a>
		</if>
		<if !$thumbnail>
			<div class="nothumb"><a target="_blank" href="<var expand_filename(clean_path($image))>"><const S_NOTHUMB></a></div>
		</if>
	</if>

	<blockquote>
	<var $comment>
	</blockquote>

	</td></tr></tbody></table>
</if>
});




use constant DELETED_TEMPLATE => compile_template( q{
});



use constant BACKLOG_PAGE_TEMPLATE => compile_template( NORMAL_HEAD_INCLUDE.q{
}.NORMAL_FOOT_INCLUDE);



use constant RSS_TEMPLATE => compile_template( q{
});



use constant ERROR_TEMPLATE => compile_template(NORMAL_HEAD_INCLUDE.q{

<h1 style="text-align: center"><var $error><br /><br />
<a href="<var escamp($ENV{HTTP_REFERER})>"><const S_RETURN></a><br /><br />
</h1>

}.NORMAL_FOOT_INCLUDE);



sub get_filename($) { my $path=shift; $path=~m!([^/]+)$!; clean_string($1) }

1;
