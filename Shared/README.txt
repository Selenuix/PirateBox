
			</div>
		</div>
		<footer id="about">
			<div class="container">
				<p class="to-top"><a href="#header">Retour en haut</a></p>
					<h2>À propos</h2>
						<p>Inspiré par les radios pirate et le mouvement de la culture libre, PirateBox est un dispositif mobile autonome de collaboration et de partage de fichiers. PirateBox utilise des logiciels libres pour créer des réseaux mobiles sans fils de partage de fichiers dont les utilisateurs peuvent partager de manière anonyme des images, des vidéos, de l'audio, des documents ou tout autre contenu numérique.</p>
						<p>PirateBox est conçue pour être sûre et sécurisée. Aucun compte utilisateur n'est requis et aucune donnée utilisateur n'est sauvegardée. Le système n'est volontairement pas connecté à Internet afin de prévenir toute tentative de suivi et de préserver la vie privée de ses utilisateurs.</p>
						<small> PirateBox est distribuée sous licence GPLv3. </small>
			</div>
		</footer>
	</body>
</html>