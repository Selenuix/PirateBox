#!/bin/sh

# matthias.strubel@aod-rpg.de
#
# Le script suivant est utilisé pour distribuer un fichier spécifique dans les répertoires du dossier donné

directory=$1
src_file=$2
overwrite=$3
overwrite=${overwrite:=false}

# Pour activer le mode DEBUG, exécutez la ligne suivante avant de lancer ce script
#   export DEBUG=true
DEBUG=${DEBUG:=false}

TEST_RUN=false

filename="${src_file##*/}"

 $DEBUG && echo "filename: $filename"
 $DEBUG && echo "Overwrite mode : $overwrite "

if [ ! -e "$directory/$filename" ] || [ "$overwrite" = true ] ; then
	echo "Distribute $filename into $directory "
 	$DEBUG && echo "	cp $src_file $directory "
	$TEST_RUN ||  cp "$src_file" "$directory"  
else
	$DEBUG && echo "File exists"
fi