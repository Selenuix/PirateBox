#
# Exemple de fichier de configuration
# 
# Décommentez et éditez les options que vous voulez changer
# les valeurs par défaut. Vous devez spécifier ADMIN_PASS et SECRET
#

# Configuration système
#use constant ADMIN_PASS => 'CHANGEME';			# Mot de passe administrateur. A changer
#use constant SECRET => 'CHANGEME';				# Phrase de passe. CHANGER LA et mettez quelque chose d'aléatoire et de long.
## TODO: Traduire cette phrase 
#use constant CAPPED_TRIPS => ('!!example1'=>' capcode','!!example2'=>' <em>cap</em>');	# Admin tripcode hash, for startng threads when locked down, and similar. Format is '!trip'=>'capcode', where 'capcode' is what is shown instead of the trip. This can contain HTML, but keep it valid XHTML!

# Apparence de la page
#use constant TITLE => 'Kareha image board';	# Nom de ce tableau d'images
#use constant SHOWTITLETXT => 1;				# Afficher le TITRE en haut (1: oui 0: non)
#use constant SHOWTITLEIMG => 0;				# Afficher l'image en haut (0: non, 1: simple, 2: en rotation)
#use constant TITLEIMG => 'title.jpg';          # Titre de l'image (pointe vers un script si rotation)
#use constant THREADS_DISPLAYED => 10;			# Nombre de fils sur la première page
#use constant THREADS_LISTED => 40;				# Nombre de threads dans la liste de threads
#use constant REPLIES_PER_THREAD => 10;			# Réponses affichées
#use constant S_ANONAME => 'Anonymous';			# Définit ce qu'il faut imprimer s'il n'y a pas de texte saisi dans le champ de nom
#use constant DEFAULT_STYLE => 'Futaba';		# Feuille de style par défaut
#use constant FAVICON => 'kareha.ico';			# Chemin d'accès au favicon pour le tableau

# Limites
#use constant ALLOW_TEXT_THREADS => 1;			# Autoriser les utilisateurs à créer des sujets
#use constant ALLOW_TEXT_REPLIES => 1;			# Autoriser les utilisateurs à répondre aux sujets
#use constant AUTOCLOSE_POSTS => 0;				# Nombre maximal de messages avant qu'un sujet ne se ferme. 0 pour désactiver.
#use constant AUTOCLOSE_DAYS => 0;				# Nombre maximal de jours sans activité avant qu'un fil ne se ferme. 0 pour désactiver.
#use constant AUTOCLOSE_SIZE => 0;				# Taille maximale du fichier HTML en kilobytes avant qu'un thread ne se ferme. 0 pour désactiver.
## TODO: Traduire cette phrase
#use constant MAX_RES => 20;					# Maximum topic bumps
#use constant MAX_THREADS => 0;					# Nombre maximum de sujets. 0 pour désactiver.
#use constant MAX_POSTS => 500;					# Nombre maximum de messages. 0 pour désactiver.
#use constant MAX_MEGABYTES => 0;				# Taille maximale à utiliser pour toutes les images en mégaoctets - 0 pour désactiver
#use constant MAX_FIELD_LENGTH => 100;			# Nombre maximum de caractères dans le sujet, le nom et le courrier électronique
#use constant MAX_COMMENT_LENGTH => 8192;		# Nombre maximum de caractères dans un commentaire.
#use constant MAX_LINES_SHOWN => 15;			# Lignes maximales d'un commentaire affiché sur la page principale (0 = pas de limite)
#use constant ALLOW_ADMIN_EDIT => 0;			# Autoriser l'édition des fichiers d'inclusion et du spam.txt depuis admin.pl.
                                    			# Attention ! Il s'agit d'un risque de sécurité, car les modèles d'inclusion peuvent générer du code ! N'acceptez que si vous confiez complètement vos modérateurs !

# Messages d'images
#use constant ALLOW_IMAGE_THREADS => 1;			# Autoriser les utilisateurs à créer des fils d'images.
#use constant ALLOW_IMAGE_REPLIES => 1;			# Autoriser les utilisateurs à répondre par des images.
#use constant IMAGE_REPLIES_PER_THREAD => 0;	# Nombre de réponses d'image par fil à afficher, mis à 0 pour aucune limite.
#use constant MAX_KB => 1000;					# Taille d'envoi maximale en KB.
#use constant MAX_W => 200;						# Les images dépassant cette largeur seront en miniatures.
#use constant MAX_H => 200;						# Les images dépassant cette hauteur seront en miniature.
#use constant THUMBNAIL_SMALL => 1;				# Miniature de petites images (1: oui, 0: non).
#use constant THUMBNAIL_QUALITY => 70;			# Qualité des miniatures JPEG (de 0 à 100).
#use constant ALLOW_UNKNOWN => 0;				# Autoriser des types de fichiers inconnus (1: oui, 0: non).
## TODO: Traduire cette phrase.
#use constant MUNGE_UNKNOWN => '.unknown';		# Munge unknown file type extensions with this. If you remove this, make sure your web server is locked down properly.
#use constant FORBIDDEN_EXTENSIONS => ('php','php3','php4','phtml','shtml','cgi','pl','pm','py','r','exe','dll','scr','pif','asp','cfm','jsp','vbs'); # Extensions de fichiers interdites.
#use constant STUPID_THUMBNAILING => 0;			# Ignorez le code miniature et utilisez simplement HTML pour redimensionner l'image. STUPIDE, détruit la bande passante. (1: activé, 0: desactivé)
#use constant MAX_IMAGE_WIDTH => 16384;			# Largeur maximale de l'image avant de rejeter.
#use constant MAX_IMAGE_HEIGHT => 16384;		# Hauteur maximale d'image avant de rejeter.
#use constant MAX_IMAGE_PIXELS => 50000000;		# Largeur * hauteur maximale de l'image avant de rejeter.
#use constant CONVERT_COMMAND => 'convert';		# emplacement de la commande convert d'ImageMagick (généralement simplement «convertir», mais parfois un chemin complet est nécessaire).

# Captcha
#use constant ENABLE_CAPTCHA => 0;				# Activer les codes de vérification (0: désactivé, 1: activé).
#use constant CAPTCHA_HEIGHT => 18;				# Hauteur approximative du captcha.
#use constant CAPTCHA_SCRIBBLE => 0.2;			# Facteur de gribouillage.
#use constant CAPTCHA_SCALING => 0.15;			# Facteur de mise à l'échelle aléatoire.
#use constant CAPTCHA_ROTATION => 0.3;			# Facteur de rotation aléatoire.
#use constant CAPTCHA_SPACING => 2.5;			# Espace entre les lettres.

# Personnalisation
#use constant CHARSET => 'utf-8';				# Le jeu de caractères à utiliser, généralement "utf-8" ou "shift_jis". N'oubliez pas de configurer Apache pour utiliser le même jeu de caractères pour les fichiers .html ! (AddCharset shift_jis html)
#use constant PROXY_CHECK => ();				# Ports du proxy à scanner - PAS ENCORE IMPLEMENTÉ !
#use constant TRIM_METHOD => 0;					# Quels fils à couper (0: le plus ancien - comme futaba 1: le moins actif - le plus en arrière).
#use constant REQUIRE_THREAD_TITLE => 0;		# Exiger un titre pour les discussions (0: non, 1: oui).
#use constant DATE_STYLE => 'futaba';			# Format de la date ('2ch', 'futaba', 'localtime, 'http').
#use constant DISPLAY_ID => '';					# Afficher les IDs utilisateur (0 ou '' : ne pas afficher,
												#  'day', 'thread', 'board' dans n'importe quelle combinaison: change les IDs pour chaque jour, fil ou tableau,
												#  'mask': afficher une adresse IP masquée (les IP semblables semblent similaires, mais sont encore chiffrés)
##TODO: Traduire cette ligne
                                                #  'sage': don't display ID when user sages, 'link': ne pas afficher l'ID lorsque l'utilisateur remplit le champ du lien,
												#  'ip': Afficher l'IP, 'host': afficher l'hôte de l'utilisateur).
#use constant EMAIL_ID => 'Heaven';				# Remplacez l'ID par cette chaîne lorsque l'utilisateur utilise un courrier électronique. Définissez sur '' pour désactiver.
#use constant SILLY_ANONYMOUS => '';			# Configurez des noms stupides pour les personnes anonymes (même syntaxe que DISPLAY_ID).
#use constant FORCED_ANON => 0;					# Forcer une publication anonyme (0: non, 1: oui).
##TODO: traduire cette phrase
#use constant TRIPKEY => '!';					# This character is displayed before tripcodes
#use constant ALTERNATE_REDIRECT => 0;			# Utilisez une méthode de redirection alternative. (Javascript au lieu de HTTP).
#use constant APPROX_LINE_LENGTH => 150;		# Longueur de ligne approximative utilisée par le code abréviation de réponse pour deviner la longueur d'une réponse.
#use constant COOKIE_PATH => 'root';			# L'argument du chemin pour les cookies ('root': les cookies s'appliquent à tous les tableaux du site, 'current': les cookies s'appliquent uniquement à ce tableau, 'parent': les cookies s'appliquent à tous les tableaux dans le répertoire parent) - NE s'applique PAS au style cookie !
#use constant STYLE_COOKIE => 'wakabastyle';	# Nom du cookie pour le sélecteur de style.
#use constant ENABLE_DELETION => 1;				# Activer la suppression des messages par les utilisateurs. (0: non, 1: oui).
#use constant PAGE_GENERATION => 'paged';		# Méthode de génération de page ('single': une seule page, 'paged': divisé en plusieurs pages comme futaba, 'monthly': pages séparées pour chaque mois).
#use constant DELETE_FIRST => 'remove';			# Que faire lorsque le premier message est supprimé ('keep': maintenez le thread, 'single': supprimez le thread s'il n'y a qu'un seul article, 'remove': supprimez le thread entier).
#use constant DEFAULT_MARKUP => 'waka';			# Format de balisage par défaut ('none', 'waka', 'html', 'aa').
#use constant FUDGE_BLOCKQUOTES => 1;			# Modifier le formatage des anciennes feuilles de style.
#use constant USE_XHTML => 1;					# Envoyer des pages comme application / xhtml + xml aux navigateurs qui prennent en charge cette fonction (0: non, 1: oui).
#use constant KEEP_MAINPAGE_NEWLINES => 0;		# Ne retirez pas l'espace blanc de la page principale (nécessaire pour que les annonces Google fonctionnent, 0: non, 1: oui).
#use constant SPAM_TRAP => 1;					# Activer le piège anti-spam (champs de formulaire vide et caché que les robots de spam remplissent habituellement) (0: non, 1: oui).

# Les chemins et les fichiers internes - peuvent également laisser cela seul.
#use constant RES_DIR => 'res/';				# Répondre au répertoire du cache (doit être écrit par le script).
#use constant CSS_DIR => 'css/';				# Dossier contenant les fichiers CSS.
#use constant IMG_DIR => 'src/';				# Dossier contenant les images (doit avoir des droits d'écriture).
#use constant THUMB_DIR => 'thumb/';			# Dossier contenant les miniatures (doit avoir des droits d'écriture).
#use constant INCLUDE_DIR => 'include/';		# Dossier des includes.
#use constant LOG_FILE => 'log.txt';			# Fichier de log (contient les mot de passe de suppression de posts et les adresses IP sous forme chiffrée).
#use constant PAGE_EXT => '.html';				# Extension utilisée pour les pages du tableau après la première.
#use constant HTML_SELF => 'index.html';		# Nom du fichier HTML principal.
#use constant HTML_BACKLOG => '';				# Nom du fichier HTML arriéré.
#use constant RSS_FILE => '';					# Fichier RSS, mettre à '' pour désactiver.
#use constant JS_FILE => 'kareha.js';			# Emplacement du fichier JS.
#use constant SPAM_FILES => ('spam.txt');		# Fichiers de définitions SPAM.
                                                # Conseil : * Définissez tous les tableaux pour utiliser le même fichier pour une mise à jour facile.
                                                #           * Configurez deux fichiers, l'un étant la liste officielle de
                                                #           * http://wakaba.c3.cx/antispam/spam.txt, et un de vos ajouts.

# Options administrateur
#use constant ADMIN_SHOWN_LINES => 5;				# Nombre de lignes de publication que le script d'administration montre.
#use constant ADMIN_SHOWN_POSTS => 10;				# Nombre de messages par thread que montre le script d'administration.
#use constant ADMIN_MASK_IPS => 0;					# Masquer les adresses IP de l'affiche dans le script d'administration (0: non, 1: oui)
#use constant ADMIN_EDITABLE_FILES => (SPAM_FILES); # Une liste Perl de tous les fichiers pouvant être édités à partir du script d'administration.
                                                    # Conseils : * Si vous ne faites pas confiance à vos modérateurs, ne les laissez pas modifier les modèles !
                                                    #            * Les modèles peuvent exécuter du code sur votre serveur !
                                                    #            * Si vous souhaitez toujours autoriser l'édition de modèles, utilisez
                                                    #            * (SPAM_FILES,glob("include/*")) comme une sténographie pratique.
#use constant ADMIN_BAN_FILE => '.htaccess';		# Nom du fichier pour écrire des bans à
#use constant ADMIN_BAN_TEMPLATE => "\n# Banni à <var scalar localtime> (<var \$reason>)\nRefusé par <var \$ip>\n";
													# Format des entrées de ban, en utilisant la syntaxe du modèle.


# Icônes pour les types de fichiers - les extensions de fichier spécifiées ici ne seront pas renommées et obtiendront des icônes
# (à l'exception des formats d'image intégrés). Ces icônes d'exemple peuvent être trouvées dans le répertoire extras/.
# Utilisez la constante FILETYPES => (
#   # Fichiers audio
#	mp3 => 'icons/audio-mp3.png',
#	ogg => 'icons/audio-ogg.png',
#	aac => 'icons/audio-aac.png',
#	m4a => 'icons/audio-aac.png',
#	mpc => 'icons/audio-mpc.png',
#	mpp => 'icons/audio-mpp.png',
#	mod => 'icons/audio-mod.png',
#	it => 'icons/audio-it.png',
#	xm => 'icons/audio-xm.png',
#	fla => 'icons/audio-flac.png',
#	flac => 'icons/audio-flac.png',
#	sid => 'icons/audio-sid.png',
#	mo3 => 'icons/audio-mo3.png',
#	spc => 'icons/audio-spc.png',
#	nsf => 'icons/audio-nsf.png',
#	# Fichiers archive
#	zip => 'icons/archive-zip.png',
#	rar => 'icons/archive-rar.png',
#	lzh => 'icons/archive-lzh.png',
#	lha => 'icons/archive-lzh.png',
#	gz => 'icons/archive-gz.png',
#	bz2 => 'icons/archive-bz2.png',
#	'7z' => 'icons/archive-7z.png',
#	# Autres fichiers
#	swf => 'icons/flash.png',
#	torrent => 'icons/torrent.png',
#	# Pour empêcher Wakaba de renommer les fichiers d'image, mettez leur nom ici comme ceci :
#	gif => '.',
#	jpg => '.',
#	png => '.',
#);

# Tags et attributs HTML autorisés. Un certain nombre de non documenté pour l'instant, mais n'hésitez pas à
# apprendre par l'exemple.
# Utilisez la constante ALLOWED_HTML => (
#	'a'=>{args=>{'href'=>'url'},forced=>{'rel'=>'nofollow'}},
#	'b'=>{},'i'=>{},'u'=>{},'sub'=>{},'sup'=>{},
#	'em'=>{},'strong'=>{},
#	'ul'=>{},'ol'=>{},'li'=>{},'dl'=>{},'dt'=>{},'dd'=>{},
#	'p'=>{},'br'=>{empty=>1},'blockquote'=>{},
#);


1;
