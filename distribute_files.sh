#!/bin/sh

# Matthias Strubel (c) 2014  - GPL3
# matthias.strubel@aod-rpg.de
#
# Ce script distribue un ensemble de fichiers au dossier $ 1

# Script pour des trucs simples
PIRATEBOX_FOLDER=$4
PIRATEBOX_FOLDER=${PIRATEBOX_FOLDER:=/opt/piratebox}
script=$PIRATEBOX_FOLDER/bin/distribute_file_into_directory.sh


# Pour activer le mode DEBUG, exécutez la ligne suivante avant de lancer ce script
# export DEBUG=true
DEBUG=${DEBUG:=false}

work_on_file() {
  local destination_root_folder=$1
  local src_file=$2

  find $destination_root_folder -type d -exec $script "{}"  $src_file $overwrite ';'
  
}



#-------------

destination=$1
overwrite=$2
overwrite=${overwrite:=false}
src_file=$3
src_file=${src_file:="all"}

$DEBUG && echo "paramètres:
  destination $destination
  écraser $overwrite
  src_file $src_file
  PirateBox_dossier=$PIRATEBOX_FOLDER
  script d'appel: $script
 ";

if [ "$src_file" = "all" ] ; then
	work_on_file $destination $PIRATEBOX_FOLDER/src/HEADER.txt
	work_on_file $destination $PIRATEBOX_FOLDER/src/README.txt
else 
	work_on_file $destination $src_file
fi


